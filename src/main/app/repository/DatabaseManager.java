package main.app.repository;

import main.app.AppConfig;
import main.app.models.Sale;
import main.app.models.category.Category;
import main.app.models.Product;
import main.app.tools.LogTool;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DatabaseManager implements AppConfig{

    String CREATE_TABLE_CATEGORIES = "CREATE TABLE IF NOT EXISTS " + TABLE_CATEGORIES + " (" +
            COLUMN_CAT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            COLUMN_CAT_TITLE + " TEXT);";

    String CREATE_TABLE_PRODUCTS = "CREATE TABLE IF NOT EXISTS " + TABLE_PRODUCTS + "(" +
            COLUMN_PRODUCT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            COLUMN_PRODUCT_CAT_ID + " INTEGER," +
            COLUMN_PRODUCT_TITLE + " TEXT," +
            COLUMN_PRODUCT_DESC + " TEXT," +
            COLUMN_PRODUCT_PRICE + " INTEGER," +
            COLUMN_PRODUCT_IMAGE + " TEXT);";

    String CREATE_TABLE_SALES = "CREATE TABLE IF NOT EXISTS " + TABLE_SALES + "(" +
            COLUMN_SALE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            COLUMN_SALE_PRODUCT_ID + " INTEGER," +
            COLUMN_SALE_PRODUCT_CAT_ID + " INTEGER," +
            COLUMN_SALE_PRODUCT_TITLE + " TEXT," +
            COLUMN_SALE_PRODUCT_DESC + " TEXT," +
            COLUMN_SALE_PRODUCT_PRICE + " INTEGER," +
            COLUMN_SALE_PRODUCT_IMAGE + " TEXT," +
            COLUMN_SALE_QUANTITY + " INTEGER);";

    String INSERT_CATEGORY = "INSERT INTO " + TABLE_CATEGORIES + "('" + COLUMN_CAT_TITLE + "')" + " VALUES(?);";

    String INSERT_SALE = "INSERT INTO " + TABLE_SALES +
            "(" + COLUMN_SALE_PRODUCT_ID + ", " +
            COLUMN_SALE_PRODUCT_CAT_ID + ", " +
            COLUMN_SALE_PRODUCT_TITLE + ", " +
            COLUMN_SALE_PRODUCT_DESC + ", " +
            COLUMN_SALE_PRODUCT_PRICE + ", " +
            COLUMN_SALE_PRODUCT_IMAGE + ", " +
            COLUMN_SALE_QUANTITY + ") VALUES(?, ?, ?, ?, ?, ?, ?);";

    String INSERT_PRODUCT = "INSERT INTO " + TABLE_PRODUCTS +
            "('" + COLUMN_PRODUCT_CAT_ID + "', '" + COLUMN_PRODUCT_TITLE + "', '" + COLUMN_PRODUCT_DESC + "', '" +
            COLUMN_PRODUCT_PRICE + "', '" + COLUMN_PRODUCT_IMAGE + "') VALUES(?, ?, ?, ?, ?);";

    String SELECT_CATEGORIES = "SELECT " + COLUMN_CAT_ID + ", " + COLUMN_CAT_TITLE + " FROM " + TABLE_CATEGORIES + ";";

    String SELECT_PRODUCTS = "SELECT * FROM " +
            TABLE_PRODUCTS + ";";

    String SELECT_SALES = "SELECT * FROM " +
            TABLE_SALES + ";";

    String SELECT_PRODUCTS_BY_CAT = "SELECT * FROM " + TABLE_PRODUCTS + " WHERE " + COLUMN_PRODUCT_CAT_ID + " = ";
    String SELECT_SALES_BY_CAT = "SELECT * FROM " + TABLE_SALES + " WHERE " + COLUMN_SALE_PRODUCT_CAT_ID + " = ";
    String SELECT_PRODUCT_BY_ID = "SELECT * FROM " + TABLE_PRODUCTS + " WHERE " + COLUMN_PRODUCT_ID + " = ";
    String SELECT_CATEGORY_BY_ID = "SELECT * FROM " + TABLE_CATEGORIES + " WHERE " + COLUMN_CAT_ID + " = ";

    String UPDATE_PRODUCT_BY_ID = "UPDATE " + TABLE_PRODUCTS + " SET (" +
            COLUMN_PRODUCT_CAT_ID + ", " +
            COLUMN_PRODUCT_TITLE + ", " +
            COLUMN_PRODUCT_DESC + ", " +
            COLUMN_PRODUCT_PRICE + ", " +
            COLUMN_PRODUCT_IMAGE +
            ") = (?, ?, ?, ?, ?) WHERE " + COLUMN_PRODUCT_ID + " = ";

    String DELETE_PRODUCT = "DELETE FROM " + TABLE_PRODUCTS + " WHERE " + COLUMN_PRODUCT_ID + " = ?";

    private static Connection connection;;
    private static DatabaseManager instance;

    public static DatabaseManager getInstance() {
        if (instance == null) {
            instance = new DatabaseManager();
        }
        return instance;
    }

    public void createDB(){
        System.out.println("Connection null " + (connection == null));
        try {
            if (connection == null){
                connection = DriverManager.getConnection(DATABASE_PATH);
                System.out.println("Connection null " + (connection == null));
            }
        }catch (SQLException e){
            System.out.println("Error on opening connection: " + e.getMessage());
        }
    }

    public void closeDB(){
        try {
            if (connection != null){
                connection.close();
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }

    public void createTables() {
        try {
            Statement statement = connection.createStatement();
            boolean cats = statement.execute(CREATE_TABLE_CATEGORIES);
            boolean products = statement.execute(CREATE_TABLE_PRODUCTS);
            boolean sales = statement.execute(CREATE_TABLE_SALES);
            System.out.println("Tables created " + cats + "\n " + products + "\n" + sales);
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }

    public void insertSampleCats(){
        if(getCategories().isEmpty()){
            System.out.println("Inserting cats");
            insertCategory("Snacks");
            insertCategory("Drinks");
            insertCategory("Clothes");
        }else {
            System.out.println("Sample cats already inserted");
        }
    }

    public void insertSampleProducts(){
        if (getProducts().isEmpty()){
            System.out.println("Inserting products");
            List<Category> categories = getCategories();
            Category snacks = categories.get(0);
            Category drinks = categories.get(1);
            Category clothes = categories.get(2);

            insertProduct(
                    snacks.getId(),
                    "Snickers",
                    "Snickers super nutritious bar",
                    5000,
                    getClass().getResource("/main/res/raw/snickers.jpg").toExternalForm());

            insertProduct(snacks.getId(),
                    "Mars",
                    "Mars bar"
                    , 5000,
                    getClass().getResource("/main/res/raw/mars.png").toExternalForm());

            insertProduct(drinks.getId(),
                    "Coca Cola",
                    "Coca Colcaaaa"
                    , 2000,
                    getClass().getResource("/main/res/raw/cola.jpg").toExternalForm());

            insertProduct(drinks.getId(),
                    "Sprite",
                    "Sprite Lime"
                    , 7000,
                    getClass().getResource("/main/res/raw/sprite.jpg").toExternalForm());

            insertProduct(clothes.getId(),
                    "Converse",
                    "Converse All stars"
                    , 550000,
                    getClass().getResource("/main/res/raw/converse.jpg").toExternalForm());

            insertProduct(snacks.getId(),
                    "Shirt",
                    "Blue shirt"
                    , 150000,
                    getClass().getResource("/main/res/raw/shirt.jpg").toExternalForm());
        }else {
            System.out.println("Sample products already inserted");
        }

    }

    public void insertCategory(String title){
        try {
            PreparedStatement statement = connection.prepareStatement(INSERT_CATEGORY);
            statement.setString(1, title);
            statement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void insertProduct(int catId, String title, String desc, int price, String image){
        try {
            PreparedStatement statement = connection.prepareStatement(INSERT_PRODUCT);
            statement.setInt(1, catId);
            statement.setString(2, title);
            statement.setString(3, desc);
            statement.setInt(4, price);
            statement.setString(5, image);
            statement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Inserting product " + e.getMessage());
        }
    }

    public void insertSale(int productId, int catId, String title, String desc, int price, String image, int quantity){
        try {
            PreparedStatement statement = connection.prepareStatement(INSERT_SALE);
            statement.setInt(1, productId);
            statement.setInt(2, catId);
            statement.setString(3, title);
            statement.setString(4, desc);
            statement.setInt(5, price);
            statement.setString(6, image);
            statement.setInt(7, quantity);
            statement.executeUpdate();
        } catch (SQLException e) {
            LogTool.show(e);
        }
    }

    public void editProduct(int id, int catId, String title, String desc, int price, String image) {
        try {
            String sql = UPDATE_PRODUCT_BY_ID + id;
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, catId);
            statement.setString(2, title);
            statement.setString(3, desc);
            statement.setInt(4, price);
            statement.setString(5, image);
            statement.executeUpdate();
        } catch (SQLException e) {
            LogTool.show(e);
        }
    }

    public void deleteProduct(int id){
        try {
            PreparedStatement statement = connection.prepareStatement(DELETE_PRODUCT);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            LogTool.show(e);
        }
    }

    public List<Category> getCategories(){
        List<Category> items = new ArrayList<>();
        try {
            System.out.println("Connection null " + (connection == null));
            Statement statement = connection.createStatement();
            ResultSet set = statement.executeQuery(SELECT_CATEGORIES);
            while (set.next()){
                Category category = new Category();
                category.setId(set.getInt(COLUMN_CAT_ID));
                category.setTitle(set.getString(COLUMN_CAT_TITLE));
                items.add(category);
            }
        } catch (SQLException e) {
            LogTool.show(e);
        }
        return items;
    }

    public List<Product> getProducts(){
        try {
            Statement statement = connection.createStatement();
            ResultSet set = statement.executeQuery(SELECT_PRODUCTS);
            return provideProductsList(set);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Product> getProductsByCategory(int categoryId){
        if (categoryId == -1){
            return getProducts();
        }

        try {
            String sql = SELECT_PRODUCTS_BY_CAT + categoryId;
            Statement statement = connection.createStatement();
            ResultSet set = statement.executeQuery(sql);
            return provideProductsList(set);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private List<Product> provideProductsList(ResultSet set) throws SQLException{
        List<Product> items = new ArrayList<>();
        while (set.next()){
            int id = set.getInt(COLUMN_PRODUCT_ID);
            int catId = set.getInt(COLUMN_PRODUCT_CAT_ID);
            String title = set.getString(COLUMN_PRODUCT_TITLE);
            String desc = set.getString(COLUMN_PRODUCT_DESC);
            int price = set.getInt(COLUMN_PRODUCT_PRICE);
            String image = set.getString(COLUMN_PRODUCT_IMAGE);
            Product product = new Product(id, catId, title, desc, price, image);
            items.add(product);
        }
        return items;
    }

    public List<Sale> getSales(){
        try {
            Statement statement = connection.createStatement();
            ResultSet set = statement.executeQuery(SELECT_SALES);
            return provideSalesList(set);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Sale> getSalesByCategory(int categoryId){
        try {
            String sql = SELECT_SALES_BY_CAT + categoryId;
            Statement statement = connection.createStatement();
            ResultSet set = statement.executeQuery(sql);
            return provideSalesList(set);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private List<Sale> provideSalesList(ResultSet set) throws SQLException{
        List<Sale> items = new ArrayList<>();
        while (set.next()){
            int id = set.getInt(COLUMN_SALE_ID);
            int quantity = set.getInt(COLUMN_SALE_QUANTITY);
            int productId = set.getInt(COLUMN_SALE_PRODUCT_ID);
            int catId = set.getInt(COLUMN_SALE_PRODUCT_CAT_ID);
            String title = set.getString(COLUMN_SALE_PRODUCT_TITLE);
            String desc = set.getString(COLUMN_SALE_PRODUCT_DESC);
            int price = set.getInt(COLUMN_SALE_PRODUCT_PRICE);
            String image = set.getString(COLUMN_SALE_PRODUCT_IMAGE);
            Sale sale = new Sale(id, productId, catId, title, desc, price, image, quantity);
            items.add(sale);
        }
        return items;
    }

    public Category getCategoryById(int id) {
        try {
            Statement statement = connection.createStatement();
            ResultSet set = statement.executeQuery(SELECT_CATEGORY_BY_ID + id);
            if (set.next()){
                Category category = new Category();
                category.setId(set.getInt(COLUMN_CAT_ID));
                category.setTitle(set.getString(COLUMN_CAT_TITLE));
                return category;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public Product getProduct(int id){
        try {
            String sql = SELECT_PRODUCT_BY_ID + id;
            Statement statement = connection.createStatement();
            ResultSet set = statement.executeQuery(sql);
            Product product = new Product();
            while (set.next()){
                int catId = set.getInt(COLUMN_PRODUCT_CAT_ID);
                String title = set.getString(COLUMN_PRODUCT_TITLE);
                String desc = set.getString(COLUMN_PRODUCT_DESC);
                int price = set.getInt(COLUMN_PRODUCT_PRICE);
                String image = set.getString(COLUMN_PRODUCT_IMAGE);
                product = new Product(id, catId, title, desc, price, image);
            }
            return product;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
