package main.app.repository.sales;

import main.app.models.Sale;

import java.util.List;

public interface SalesRepository {
    void recordSale(List<Sale> list);
    List<Sale> getSales();
    List<Sale> getSalesByCategory(int catId);
}
