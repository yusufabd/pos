package main.app.repository.sales;

import main.app.models.Product;
import main.app.models.Sale;
import main.app.repository.DatabaseManager;

import java.util.List;

public class SalesRepositoryImpl implements SalesRepository{

    DatabaseManager databaseManager = DatabaseManager.getInstance();

    @Override
    public void recordSale(List<Sale> list) {
        for(Sale sale : list){
            databaseManager.insertSale(sale.getProductId(), sale.getCategoryId(), sale.getTitle(), sale.getDescription(),
                    sale.getPrice(), sale.getImage(), sale.getQuantity());
        }
    }

    @Override
    public List<Sale> getSales() {
        return databaseManager.getSales();
    }

    @Override
    public List<Sale> getSalesByCategory(int catId) {
        return databaseManager.getSalesByCategory(catId);
    }
}
