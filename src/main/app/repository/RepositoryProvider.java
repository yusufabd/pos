package main.app.repository;

import main.app.repository.products.ProductsRepository;
import main.app.repository.products.ProductsRepositoryImpl;
import main.app.repository.sales.SalesRepository;
import main.app.repository.sales.SalesRepositoryImpl;

public class RepositoryProvider {

    static ProductsRepository sProductsRepository;
    static SalesRepository sSalesRepository;

    public static ProductsRepository getProductsRepository() {
        if (sProductsRepository == null) {
            sProductsRepository = new ProductsRepositoryImpl();
        }
        return sProductsRepository;
    }

    public static SalesRepository getSalesRepository() {
        if (sSalesRepository == null) {
            sSalesRepository = new SalesRepositoryImpl();
        }
        return sSalesRepository;
    }
}
