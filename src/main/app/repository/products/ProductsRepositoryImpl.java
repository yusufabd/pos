package main.app.repository.products;

import main.app.models.category.Category;
import main.app.models.Product;
import main.app.repository.DatabaseManager;

import java.util.List;

public class ProductsRepositoryImpl implements ProductsRepository{

    DatabaseManager databaseManager = DatabaseManager.getInstance();

    @Override
    public List<Category> getCategories() {
        List<Category> categoryList = databaseManager.getCategories();
        categoryList.add(0, new Category(-1, ""));
        return categoryList;
    }

    @Override
    public Category getCategory(int id) {
        return databaseManager.getCategoryById(id);
    }

    @Override
    public List<Product> getProducts() {
        return databaseManager.getProducts();
    }

    @Override
    public List<Product> getProductsByCategory(int id) {
        return databaseManager.getProductsByCategory(id);
    }

    @Override
    public Product getProduct(int id) {
        return databaseManager.getProduct(id);
    }

    @Override
    public void addProduct(int catId, String title, String desc, int price, String image) {
        databaseManager.insertProduct(catId, title, desc, price, image);
    }

    @Override
    public void editProduct(int id, int catId, String title, String desc, int price, String image) {
        databaseManager.editProduct(id, catId, title, desc, price, image);
    }

    @Override
    public void deleteProduct(int id) {
        databaseManager.deleteProduct(id);
    }
}
