package main.app.repository.products;

import main.app.models.category.Category;
import main.app.models.Product;

import java.util.List;

public interface ProductsRepository {
    List<Category> getCategories();
    Category getCategory(int id);

    List<Product> getProducts();
    List<Product> getProductsByCategory(int id);
    Product getProduct(int id);

    void addProduct(int catId, String title, String desc, int price, String image);
    void editProduct(int id, int catId, String title, String desc, int price, String image);
    void deleteProduct(int id);
}
