package main.app.windows.warehouse;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import main.app.controllers.WarehouseController;
import main.app.models.Product;
import main.app.models.category.Category;
import main.app.repository.RepositoryProvider;
import main.app.repository.products.ProductsRepository;
import main.app.windows.BaseWindow;


public class WarehouseWindow extends BaseWindow {

    WarehouseController controller;

    @Override
    public void startWindow() {
        super.startWindow();
        Parent parent = getParent();

        controller = (WarehouseController) getController();
        controller.setWindow(this);
        controller.setCategoryList(productsRepository.getCategories());
        controller.setProductList(productsRepository.getProducts());

        Scene scene = new Scene(parent, WINDOW_WIDTH, WINDOW_HEIGHT);

        stage = new Stage();
        stage.resizableProperty().setValue(Boolean.FALSE);
        stage.setTitle("Warehouse");
        stage.setScene(scene);
        stage.show();

        controller.initCategoriesBox();
        controller.initTableView();
        controller.inflateTableView();
    }

    public void refresh(){
        controller.setProductList(productsRepository.getProducts());
        controller.inflateTableView();
    }

    @Override
    public String getLayoutPath() {
        return "/main/res/layout/warehouse.fxml";
    }

    public void startAddWindow() {
        AddEditProductWindow addWindow = new AddEditProductWindow();
        addWindow.setPrevWindow(this);
        addWindow.startWindow();
    }

    public void startEditWindow(int id){
        AddEditProductWindow editWindow = new AddEditProductWindow();
        editWindow.setPrevWindow(this);
        editWindow.setEdit(true);
        editWindow.setEditProductId(id);
        editWindow.startWindow();
    }

    public Category getCategory(int id){
        return productsRepository.getCategory(id);
    }

    public void onCategoryChanged(Category newValue) {
        controller.setProductList(productsRepository.getProductsByCategory(newValue.getId()));
        controller.inflateTableView();
    }

    public void deleteProduct(Product focusedProduct) {
        productsRepository.deleteProduct(focusedProduct.getId());
    }
}
