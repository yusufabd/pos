package main.app.windows.warehouse;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import main.app.controllers.AddProductController;
import main.app.models.Product;
import main.app.repository.RepositoryProvider;
import main.app.repository.products.ProductsRepository;
import main.app.windows.BaseWindow;

public class AddEditProductWindow extends BaseWindow{

    private boolean isEdit = false;
    private int editProductId = 0;
    private WarehouseWindow prevWindow;

    @Override
    public void startWindow() {
        super.startWindow();
        Parent parent = getParent();
        AddProductController controller = (AddProductController) getController();
        controller.setWindow(this);

        Scene scene = new Scene(parent, WINDOW_WIDTH, WINDOW_HEIGHT);
        stage = new Stage();
        stage.resizableProperty().setValue(Boolean.FALSE);
        stage.setTitle("Add product");
        stage.setScene(scene);
        stage.show();

        controller.setCategoryList(productsRepository.getCategories());
        controller.initFields();

        if (isEdit){
            Product product = productsRepository.getProduct(editProductId);
            controller.inflateFields(product);
        }
    }

    @Override
    public String getLayoutPath() {
        return "/main/res/layout/add_product.fxml";
    }

    public void addProduct(String title, String desc, int catId, int price, String image) {
        if (isEdit){
            productsRepository.editProduct(editProductId, catId, title, desc, price, image);
        }else {
            productsRepository.addProduct(catId, title, desc, price, image);
        }
        prevWindow.refresh();
        closeWindow();
    }

    public void setEdit(boolean edit) {
        isEdit = edit;
    }

    public void setEditProductId(int editProductId) {
        this.editProductId = editProductId;
    }

    public void setPrevWindow(WarehouseWindow prevWindow) {
        this.prevWindow = prevWindow;
    }

    public void makeToFront() {
        stage.toFront();
    }
}
