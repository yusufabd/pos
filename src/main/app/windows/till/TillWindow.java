package main.app.windows.till;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Dialog;
import javafx.stage.Stage;
import main.app.controllers.TillController;
import main.app.models.Sale;
import main.app.models.category.Category;
import main.app.tools.BillTool;
import main.app.windows.BaseWindow;

import java.util.List;

public class TillWindow extends BaseWindow {

    TillController controller;

    @Override
    public void startWindow() {
        super.startWindow();
        Parent parent = getParent();

        System.out.println("Repository == " + (productsRepository == null));

        controller = (TillController) getController();
        controller.setWindow(this);
        controller.setCategoryList(productsRepository.getCategories());
        controller.setProductList(productsRepository.getProducts());
        controller.init();
        controller.inflateList();

        Scene scene = new Scene(parent, WINDOW_WIDTH, WINDOW_HEIGHT);

        stage = new Stage();
        stage.resizableProperty().setValue(Boolean.FALSE);
        stage.setTitle("Till");
        stage.setScene(scene);
        stage.show();
    }

    @Override
    public String getLayoutPath() {
        return "/main/res/layout/till.fxml";
    }

    public void onCategoryChanged(Category newValue) {
        controller.setProductList(productsRepository.getProductsByCategory(newValue.getId()));
        controller.inflateList();
    }

    public void submitSale(List<Sale> saleList) {
        salesRepository.recordSale(saleList);
        BillTool.generatePDF("", saleList);
    }
}
