package main.app.windows.auth;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import main.app.controllers.SignInController;
import main.app.models.users.Employee;
import main.app.repository.DatabaseManager;
import main.app.windows.BaseWindow;
import main.app.windows.sections.SectionsWindow;

import java.util.List;

public class SignInWindow extends BaseWindow {

    private List<Employee> employees;
    private Employee selectedEmployee;

    @Override
    public void startWindow() {
        super.startWindow();
        Parent parent = getParent();

        SignInController controller = (SignInController) getController();
        controller.setWindow(this);

        Scene scene = new Scene(parent, WINDOW_WIDTH, WINDOW_HEIGHT);

        stage = new Stage();
        stage.resizableProperty().setValue(Boolean.FALSE);
        stage.setTitle("Welcome!");
        stage.setScene(scene);
        stage.show();

        DatabaseManager databaseManager = new DatabaseManager();
        databaseManager.createTables();
    }

    @Override
    public String getLayoutPath() {
        return "/main/res/layout/login.fxml";
    }

    public void startSections() {
        SectionsWindow window = new SectionsWindow();
        window.setEmployee(selectedEmployee);
        window.startWindow();
        closeWindow();
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public Employee getSelectedEmployee() {
        return selectedEmployee;
    }

    public void setSelectedEmployee(Employee selectedEmployee) {
        this.selectedEmployee = selectedEmployee;
    }
}
