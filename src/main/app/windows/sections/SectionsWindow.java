package main.app.windows.sections;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import main.app.controllers.SectionsController;
import main.app.models.users.Employee;
import main.app.repository.DatabaseManager;
import main.app.windows.BaseWindow;
import main.app.windows.till.TillWindow;
import main.app.windows.warehouse.WarehouseWindow;

public class SectionsWindow extends BaseWindow {

    Employee employee;

    @Override
    public void startWindow() {
        super.startWindow();
        Parent parent = getParent();

        SectionsController controller = (SectionsController) getController();
        controller.setWindow(this);

        Scene scene = new Scene(parent, WINDOW_WIDTH, WINDOW_HEIGHT);

        stage = new Stage();
        stage.resizableProperty().setValue(Boolean.FALSE);
        stage.setTitle("Select section");
        stage.setScene(scene);
        stage.show();

        DatabaseManager databaseManager = new DatabaseManager();
        databaseManager.insertSampleCats();
        databaseManager.insertSampleProducts();

        System.out.println("Cats' count = " + databaseManager.getCategories().size());
        System.out.println("Products' count = " + databaseManager.getProducts().size());
    }

    @Override
    public String getLayoutPath() {
        return "/main/res/layout/sections.fxml";
    }

    public void startTillWindow(){
        TillWindow window = new TillWindow();
        window.startWindow();
    }

    public void startWarehouseWindow(){
        WarehouseWindow window = new WarehouseWindow();
        window.startWindow();
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Employee getEmployee() {
        return employee;
    }
}
