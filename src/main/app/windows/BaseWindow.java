package main.app.windows;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.stage.Stage;
import main.app.AppConfig;
import main.app.controllers.BaseController;
import main.app.repository.RepositoryProvider;
import main.app.repository.products.ProductsRepository;
import main.app.repository.sales.SalesRepository;

import java.io.IOException;

public abstract class BaseWindow implements AppConfig{

    protected Stage stage;
    protected Parent parent;
    protected BaseController controller;

    public ProductsRepository productsRepository;
    public SalesRepository salesRepository;

    public void startWindow(){
        productsRepository = RepositoryProvider.getProductsRepository();
        salesRepository = RepositoryProvider.getSalesRepository();
    }

    public abstract String getLayoutPath();

    public void closeWindow(){
        stage.close();
    }

    public Parent getParent(){
        FXMLLoader loader = new FXMLLoader(getClass().getResource(getLayoutPath()));
        try {
            parent = loader.load();
            controller = loader.getController();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return parent;
    }

    public BaseController getController(){
        return controller;
    }

    public Stage getStage() {
        return stage;
    }
}
