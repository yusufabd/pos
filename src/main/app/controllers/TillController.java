package main.app.controllers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.util.Callback;
import javafx.util.StringConverter;
import main.app.models.Product;
import main.app.models.Sale;
import main.app.models.category.Category;
import main.app.windows.till.TillWindow;

import java.util.ArrayList;
import java.util.List;

public class TillController extends BaseController{

    @FXML public ComboBox boxCategories;
    @FXML public ListView<Product> listProducts;
    @FXML public TableView<Sale> tableSales;
    @FXML public Button buttonAdd;
    @FXML public Button buttonRemove;
    @FXML public Button buttonPrint;

    TillWindow window;

    List<Category> categoryList;
    List<Product> productList;
    List<Sale> saleList = new ArrayList<>();

    public void onAdd() {
        Product product = listProducts.getFocusModel().getFocusedItem();
        boolean exists = false;
        for(Sale sale : saleList){
            if (sale.getProductId() == product.getId()){
                sale.setQuantity(sale.getQuantity() + 1);
                exists = true;
                break;
            }
        }

        if (!exists){
            Sale sale = new Sale();
            sale.setProductId(product.getId());
            sale.setCategoryId(product.getCategoryId());
            sale.setTitle(product.getTitle());
            sale.setDescription(product.getDescription());
            sale.setPrice(product.getPrice());
            sale.setImage(product.getImage());
            sale.setQuantity(1);
            saleList.add(sale);
        }

        inflateTable();
    }

    public void onRemove() {
        Product product = listProducts.getFocusModel().getFocusedItem();

        for(Sale sale : saleList){
            if (sale.getProductId() == product.getId()){
                if (sale.getQuantity() > 1){
                    sale.setQuantity(sale.getQuantity() - 1);
                }else {
                    saleList.remove(sale);
                }
                break;
            }
        }

        inflateTable();
    }

    public void onPrint(){
        window.submitSale(saleList);
        saleList.clear();
        inflateTable();
    }

    public void initCategoriesBox(){
        boxCategories.setConverter(new StringConverter<Category>() {
            @Override
            public String toString(Category object) {
                return object.getTitle();
            }

            @Override
            public Category fromString(String string) {
                return null;
            }
        });
        boxCategories.setCellFactory(new Callback<ListView<Category>, ListCell<Category>>() {
            @Override
            public ListCell<Category> call(ListView<Category> param) {
                return new ListCell<Category>(){
                    @Override
                    protected void updateItem(Category item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty || item == null) {
                            setText(null);
                            setGraphic(null);
                        } else {
                            setText(item.getTitle());
                        }
                    }
                };
            }
        });
        boxCategories.setItems(FXCollections.observableList(categoryList));
        boxCategories.getSelectionModel().selectFirst();
        boxCategories.valueProperty().addListener(new ChangeListener<Category>() {
            @Override
            public void changed(ObservableValue<? extends Category> observable, Category oldValue, Category newValue) {
                window.onCategoryChanged(newValue);
            }
        });
    }

    public void initListProducts(){
        listProducts.setCellFactory(new Callback<ListView<Product>, ListCell<Product>>() {
            @Override
            public ListCell<Product> call(ListView<Product> param) {
                return new ListCell<Product>(){
                    @Override
                    protected void updateItem(Product item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty || item == null) {
                            setText(null);
                            setGraphic(null);
                        }else {
                            GridPane pane = new GridPane();
                            pane.setAlignment(Pos.CENTER);
                            pane.setVgap(10);

                            ImageView imageView = new ImageView(new Image(item.getImage()));
                            imageView.setFitWidth(200);
                            imageView.setFitHeight(200);
                            Text text = new Text(item.getTitle());

                            pane.add(imageView, 0, 0);
                            pane.add(text, 0, 1);
                            setGraphic(pane);
                        }
                    }
                };
            }
        });
    }

    public void initTableSales(){
        TableColumn columnTitle = new TableColumn("TITLE");
        columnTitle.setPrefWidth(100);
        columnTitle.setCellValueFactory(new PropertyValueFactory("title"));

        TableColumn columnPrice = new TableColumn("PRICE");
        columnPrice.setPrefWidth(100);
        columnPrice.setCellValueFactory(new PropertyValueFactory("price"));

        TableColumn columnQuantity = new TableColumn("QUANTITY");
        columnQuantity.setPrefWidth(100);
        columnQuantity.setCellValueFactory(new PropertyValueFactory("quantity"));

        tableSales.getColumns().add(columnTitle);
        tableSales.getColumns().add(columnPrice);
        tableSales.getColumns().add(columnQuantity);
    }

    public void inflateList() {
        listProducts.setItems(FXCollections.observableList(productList));
    }

    public void inflateTable(){
        tableSales.setItems(FXCollections.observableList(saleList));
        tableSales.refresh();
    }

    public void setWindow(TillWindow window) {
        this.window = window;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public void init() {
        initCategoriesBox();
        initListProducts();
        initTableSales();
    }
}
