package main.app.controllers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import javafx.util.StringConverter;
import main.app.models.category.Category;
import main.app.models.Product;
import main.app.windows.warehouse.WarehouseWindow;

import java.util.List;

public class WarehouseController extends BaseController{
    @FXML
    public Button buttonAdd;
    @FXML
    public Button buttonEdit;
    @FXML
    public Button buttonDelete;
    @FXML
    public TableView<Product> tableProducts;
    @FXML
    public ComboBox<Category> boxCategories;

    WarehouseWindow window;
    List<Product> productList;
    List<Category> categoryList;

    public void setWindow(WarehouseWindow window) {
        this.window = window;
    }

    public void onEditClick(ActionEvent actionEvent) {
        Product focusedProduct = productList.get(tableProducts.getSelectionModel().getFocusedIndex());
        window.startEditWindow(focusedProduct.getId());
    }

    public void onDeleteClick(ActionEvent actionEvent) {
        Product focusedProduct = productList.get(tableProducts.getSelectionModel().getFocusedIndex());
        productList.remove(focusedProduct);
        inflateTableView();
        window.deleteProduct(focusedProduct);
    }

    public void onAddClick(ActionEvent actionEvent) {
        window.startAddWindow();
    }

    public void initCategoriesBox(){
        boxCategories.setConverter(new StringConverter<Category>() {
            @Override
            public String toString(Category object) {
                return object.getTitle();
            }

            @Override
            public Category fromString(String string) {
                return null;
            }
        });
        boxCategories.setCellFactory(new Callback<ListView<Category>, ListCell<Category>>() {
            @Override
            public ListCell<Category> call(ListView<Category> param) {
                return new ListCell<Category>(){
                    @Override
                    protected void updateItem(Category item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty || item == null) {
                            setText(null);
                            setGraphic(null);
                        } else {
                            setText(item.getTitle());
                        }
                    }
                };
            }
        });
        boxCategories.setItems(FXCollections.observableList(categoryList));
        boxCategories.getSelectionModel().selectFirst();
        boxCategories.valueProperty().addListener(new ChangeListener<Category>() {
            @Override
            public void changed(ObservableValue<? extends Category> observable, Category oldValue, Category newValue) {
                window.onCategoryChanged(newValue);
            }
        });
    }

    public void initTableView(){
        TableColumn<Product, String> columnImage = new TableColumn("IMAGE");
        columnImage.setCellValueFactory(new PropertyValueFactory("image"));
        columnImage.setPrefWidth(100);
        columnImage.setCellFactory(new Callback<TableColumn<Product, String>, TableCell<Product, String>>() {
            @Override
            public TableCell<Product, String> call(TableColumn<Product, String> param) {
                ImageView imageView = new ImageView();
                return new TableCell<Product, String>(){
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty || item == null) {
                            setText(null);
                            setGraphic(null);
                        } else {
                            HBox hBox = new HBox();
                            hBox.setAlignment(Pos.CENTER);
                            hBox.setPadding(new Insets(10, 10, 10, 10));

                            Image image = new Image(item);
                            imageView.setImage(image);
                            imageView.setFitHeight(80);
                            imageView.setFitWidth(80);
                            hBox.getChildren().add(imageView);
                            setGraphic(hBox);
                        }
                    }
                };
            }
        });
        TableColumn columnTitle = new TableColumn("TITLE");
        columnTitle.setPrefWidth(100);
        columnTitle.setCellValueFactory(new PropertyValueFactory("title"));
        TableColumn columnDesc = new TableColumn("DESCRIPTION");
        columnDesc.setPrefWidth(100);
        columnDesc.setCellValueFactory(new PropertyValueFactory("description"));
        TableColumn columnPrice = new TableColumn("PRICE");
        columnPrice.setPrefWidth(100);
        columnPrice.setCellValueFactory(new PropertyValueFactory("price"));
        TableColumn<Product, Integer> columnCat = new TableColumn<>("CATEGORY");
        columnCat.setCellValueFactory(new PropertyValueFactory("categoryId"));
        columnCat.setCellFactory(new Callback<TableColumn<Product, Integer>, TableCell<Product, Integer>>() {
            @Override
            public TableCell<Product, Integer> call(TableColumn<Product, Integer> param) {
                return new TableCell<Product, Integer>(){
                    @Override
                    protected void updateItem(Integer item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty || item == null) {
                            setText(null);
                            setGraphic(null);
                        }else {
                            Category category = window.getCategory(item);
                            setText(category.getTitle());
                        }
                    }
                };
            }
        });
        columnCat.setPrefWidth(100);

        tableProducts.getColumns().add(columnImage);
        tableProducts.getColumns().add(columnTitle);
        tableProducts.getColumns().add(columnDesc);
        tableProducts.getColumns().add(columnPrice);
        tableProducts.getColumns().add(columnCat);
    }

    public void inflateTableView(){
        tableProducts.setItems(FXCollections.observableList(productList));
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }
}
