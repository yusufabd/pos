package main.app.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import main.app.models.users.Employee;
import main.app.windows.auth.SignInWindow;

import java.util.List;

public class SignInController extends BaseController{

    @FXML
    public Text textMessage;
    @FXML
    public TextField fieldUsername;
    @FXML
    public PasswordField fieldPassword;

    SignInWindow window;

    public void onSubmitClick(ActionEvent actionEvent) {
        boolean loginFound = false,
                passwordFound = false;

        List<Employee> employees = window.getEmployees();

        String username = fieldUsername.getText().trim();
        String password = fieldPassword.getText().trim();

        for (Employee e : employees) {
            System.out.println(e.getLogin() + " " + e.getPassword());

            loginFound = e.getLogin().equals(username);
            passwordFound = e.getPassword().equals(password);

            if (loginFound && passwordFound){
                textMessage.setText("Logged in!");
                window.setSelectedEmployee(e);
                window.startSections();
                break;
            }
        }

        if (!loginFound || !passwordFound){
            textMessage.setText("No matching account found");
        }
    }

    public void setWindow(SignInWindow window) {
        this.window = window;
    }
}
