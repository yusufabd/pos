package main.app.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import main.app.models.users.Manager;
import main.app.windows.sections.SectionsWindow;

public class SectionsController extends BaseController{
    @FXML
    public Button buttonTill;
    @FXML
    public Button buttonWarehouse;
    @FXML
    public Text textMessage;

    SectionsWindow window;

    public void onTillClick(ActionEvent actionEvent) {
        window.startTillWindow();
    }

    public void onWarehouseClick(ActionEvent actionEvent) {

        if (window.getEmployee() instanceof Manager){
            window.startWarehouseWindow();
        }else {
            textMessage.setText("You do not have required privileges");
        }

    }

    public void setWindow(SectionsWindow window) {
        this.window = window;
    }
}
