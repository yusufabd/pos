package main.app.controllers;

import javafx.collections.FXCollections;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import javafx.util.StringConverter;
import main.app.models.category.Category;
import main.app.models.Product;
import main.app.windows.warehouse.AddEditProductWindow;

import javax.swing.*;
import java.io.File;
import java.net.MalformedURLException;
import java.util.List;

public class AddProductController extends BaseController{

    @FXML
    public TextField fieldTitle;
    @FXML
    public TextField fieldDesc;
    @FXML
    public TextField fieldPrice;
    @FXML
    public ImageView imageView;
    @FXML
    public Button buttonPickImage;
    @FXML
    public Button buttonAddProduct;
    @FXML
    public ComboBox<Category> boxCategories;
    @FXML
    public Text textMessage;
    @FXML
    public GridPane parent;

    AddEditProductWindow window;
    String pickedImageUrl;
    List<Category> categoryList;

    public void setWindow(AddEditProductWindow window) {
        this.window = window;
    }

    public void onPickImage() {
        FileChooser fc = new FileChooser();
        File file = fc.showOpenDialog(window.getStage());

        try {
            setPickedImageUrl(file.toURI().toURL().toExternalForm());
        } catch (MalformedURLException e) {
            System.out.println(e.getMessage());
        }
        Task task = new Task() {
            @Override
            protected Object call() throws Exception {
                Image image = new Image(pickedImageUrl);
                imageView.setImage(image);
                window.makeToFront();
                return null;
            }
        };

        new Thread(task).start();
    }

    public void onAddProduct() {
        String title = fieldTitle.getText().trim();
        String desc = fieldDesc.getText().trim();
        int catId = boxCategories.getValue().getId();
        String price = fieldPrice.getText().trim();

        if (title.isEmpty() || desc.isEmpty() || price.isEmpty()) {
            textMessage.setText("Please, fill in all fields");
            return;
        }

        if (catId == -1){
            textMessage.setText("Please, select category");
            return;
        }

        window.addProduct(title, desc, catId, Integer.parseInt(price), pickedImageUrl);
    }

    public void initFields() {
        //Populating boxCategories with data
        boxCategories.setConverter(new StringConverter<Category>() {
            @Override
            public String toString(Category object) {
                return object.getTitle();
            }

            @Override
            public Category fromString(String string) {
                return null;
            }
        });
        boxCategories.setCellFactory(new Callback<ListView<Category>, ListCell<Category>>() {
            @Override
            public ListCell<Category> call(ListView<Category> param) {
                return new ListCell<Category>(){
                    @Override
                    protected void updateItem(Category item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty || item == null) {
                            setText(null);
                            setGraphic(null);
                        } else {
                            setText(item.getTitle());
                        }
                    }
                };
            }
        });
        boxCategories.setItems(FXCollections.observableList(categoryList));
        boxCategories.getSelectionModel().selectFirst();

        //Forcing fieldPrice to accept only numeric values
        fieldPrice.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                fieldPrice.setText(newValue.replaceAll("[^\\d]", ""));
                textMessage.setText("Please, enter numeric value for price");
            }
        });
    }

    public void setPickedImageUrl(String pickedImageUrl) {
        this.pickedImageUrl = pickedImageUrl;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    public void inflateFields(Product product) {
        fieldTitle.setText(product.getTitle());
        fieldDesc.setText(product.getDescription());
        fieldPrice.setText(String.valueOf(product.getPrice()));
        imageView.setImage(new Image(product.getImage()));
        setPickedImageUrl(product.getImage());
    }
}
