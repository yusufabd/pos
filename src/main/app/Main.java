package main.app;

import javafx.application.Application;
import javafx.stage.Stage;
import main.app.models.users.Employee;
import main.app.models.users.Manager;
import main.app.models.users.Salesman;
import main.app.repository.DatabaseManager;
import main.app.windows.auth.SignInWindow;

import java.util.ArrayList;
import java.util.List;

public class Main extends Application {

    private static List<Employee> employees = new ArrayList<>();

    @Override
    public void init() throws Exception {
        super.init();

        Salesman salesman = new Salesman(1, "Alex Hunter");
        salesman.setLogin("b");
        salesman.setPassword("b");

        Manager manager = new Manager(2, "Johnny Nash");
        manager.setLogin("a");
        manager.setPassword("a");

        employees.add(salesman);
        employees.add(manager);

        DatabaseManager databaseManager = new DatabaseManager();
        databaseManager.createDB();
    }

    @Override
    public void start(Stage stage) throws Exception {
        SignInWindow window = new SignInWindow();
        window.setEmployees(employees);
        window.startWindow();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}
