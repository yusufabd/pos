package main.app;

public interface AppConfig {
    int WINDOW_WIDTH = 600;
    int WINDOW_HEIGHT = 400;


    String DATABASE_NAME = "pos.db";
    String DATABASE_PATH = "jdbc:sqlite:" + DATABASE_NAME;

    String TABLE_CATEGORIES = "categories";
    String COLUMN_CAT_ID = "id";
    String COLUMN_CAT_TITLE = "title";

    String TABLE_PRODUCTS = "products";
    String COLUMN_PRODUCT_ID = "id";
    String COLUMN_PRODUCT_CAT_ID = "cat_id";
    String COLUMN_PRODUCT_TITLE = "title";
    String COLUMN_PRODUCT_DESC = "desc";
    String COLUMN_PRODUCT_PRICE = "price";
    String COLUMN_PRODUCT_IMAGE = "image";

    String TABLE_SALES = "sales";
    String COLUMN_SALE_ID = "id";
    String COLUMN_SALE_PRODUCT_ID = "product_id";
    String COLUMN_SALE_PRODUCT_CAT_ID = "cat_id";
    String COLUMN_SALE_PRODUCT_TITLE = "title";
    String COLUMN_SALE_PRODUCT_DESC = "desc";
    String COLUMN_SALE_PRODUCT_PRICE = "price";
    String COLUMN_SALE_PRODUCT_IMAGE = "image";
    String COLUMN_SALE_QUANTITY = "quantity";
}
