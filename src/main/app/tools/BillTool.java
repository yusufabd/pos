package main.app.tools;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import main.app.models.Sale;

import java.util.Date;
import java.util.List;

public class BillTool {

    public static void generatePDF(String salesman, List<Sale> items){
        StringBuilder builder = new StringBuilder();
        int totalSum = 0;
        for(Sale s : items){
            String line = s.getTitle() + " " + s.getPrice() + " X " + s.getQuantity();
            builder.append(line);
            builder.append("\n");
            totalSum =+ s.getQuantity() * s.getPrice();
        }
        String total = "Total: " + totalSum;
        String author = "Salesman: " + salesman;

        builder.append("\n");
        builder.append(total);
        builder.append("\n");
        builder.append(" ************ ");
        builder.append("\n");
        builder.append(author);


        Dialog dialog = new Dialog();

        Text text = new Text();
        text.setText(builder.toString());
        Button button = new Button();
        button.setText("CLOSE");
        button.setOnAction(event -> dialog.close());

        VBox vBox = new VBox();
        vBox.setAlignment(Pos.CENTER);
        vBox.getChildren().add(text);
//        vBox.getChildren().add(button);

        dialog.setGraphic(vBox);
        dialog.setTitle("Sold");
        dialog.getDialogPane().getButtonTypes().add(ButtonType.CLOSE);
        dialog.show();
    }
}
