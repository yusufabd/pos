package main.app.tools;

public class LogTool {
    public static void show(Exception e){
        System.out.println(
                "Class " + e.getStackTrace()[0].getClassName()
                + "\nLine " + e.getStackTrace()[0].getLineNumber()
                + " " + e.getMessage());
    }
}
